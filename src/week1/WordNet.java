package week1;

import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.RedBlackBST;

import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

public class WordNet {
  private RedBlackBST<String, Noun> nouns;
  private Digraph G;
  private HashMap<Integer, String> synsetMap;
  private SAP sap;

  public WordNet(String synsets, String hypernyms) {
    if (synsets == null || hypernyms == null)
      throw new IllegalArgumentException();
    nouns = new RedBlackBST<>();
    synsetMap = new HashMap<>();
    In in = new In(synsets);
    int synsetId = 0;
    while (in.hasNextLine()) {
      String[] synset = in.readLine().split(",");
      synsetId = Integer.parseInt(synset[0]);
      synsetMap.put(synsetId, synset[1]);
      for (String noun : synset[1].split(" ")) {
        Noun noun1 = nouns.get(noun);
        if (noun1 == null) {
          nouns.put(noun, new Noun(synsetId));
        } else {
          noun1.addId(synsetId);
        }
      }
    }
    G = new Digraph(synsetId + 1);
    in = new In(hypernyms);
    int roots = 0;
    while (in.hasNextLine()) {
      String[] edges = in.readLine().split(",");
      if (edges.length == 1) {
        roots++;
      }
      if (roots > 1) {
        throw new IllegalArgumentException();
      }
      int syn = Integer.parseInt(edges[0]);
      for (int i = 1; i < edges.length; i++) {
        G.addEdge(syn, Integer.parseInt(edges[i]));
      }
    }
    sap = new SAP(G);
  }

  public Iterable<String> nouns() {
    return nouns.keys();
  }

  public boolean isNoun(String word) {
    if (word == null)
      throw new IllegalArgumentException();
    return nouns.contains(word);
  }

  public int distance(String nounA, String nounB) {
    Noun A = nouns.get(nounA);
    Noun B = nouns.get(nounB);
    if (A == null || B == null) {
      throw new IllegalArgumentException();
    }
    return sap.length(A.synsetIds, B.synsetIds);
  }

  public String sap(String nounA, String nounB) {
    Noun A = nouns.get(nounA);
    Noun B = nouns.get(nounB);
    if (A == null || B == null) {
      throw new IllegalArgumentException();
    }
    return synsetMap.get(sap.ancestor(A.synsetIds, B.synsetIds));
  }

  private class Noun {
    private Set<Integer> synsetIds;

    Noun(int synsetId) {
      synsetIds = new TreeSet<>();
      synsetIds.add(synsetId);
    }

    public Iterable<Integer> getSynsetIds() {
      return synsetIds;
    }

    void addId(int synsetId) {
      synsetIds.add(synsetId);
    }
  }
}

package week1;

import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;

import java.util.TreeSet;

public class SAP {
  private Digraph DG;

  public SAP(Digraph DG) {
    this.DG = new Digraph(DG);
  }

  public int length(int v, int w) {
    int ancestor = ancestor(v, w);
    if (ancestor == -1) {
      return -1;
    }
    BreadthFirstDirectedPaths bfs = new BreadthFirstDirectedPaths(DG, v);
    BreadthFirstDirectedPaths bfs1 = new BreadthFirstDirectedPaths(DG, w);
    return bfs.distTo(ancestor) + bfs1.distTo(ancestor);
  }

  public int ancestor(int v, int w) {
    BreadthFirstDirectedPaths bfs = new BreadthFirstDirectedPaths(DG, v);
    int distToW = -1;
    int distToV = -1;
    if (bfs.hasPathTo(w)) {
      distToW = bfs.distTo(w);
    }
    BreadthFirstDirectedPaths bfs2 = new BreadthFirstDirectedPaths(DG, w);
    if (bfs2.hasPathTo(v)) {
      distToV = bfs2.distTo(v);
    }

    int ancestor = -1;
    int shortestPath = Integer.MAX_VALUE;
    for (int i = 0; i < DG.V(); i++) {
      if (bfs.hasPathTo(i) && bfs2.hasPathTo(i)) {
        int distance = bfs.distTo(i) + bfs2.distTo(i);
        if (distance < shortestPath) {
          shortestPath = distance;
          ancestor = i;
        }
      }
    }

    if (distToV > 0 && distToV < shortestPath) {
      return v;
    } else if (distToW > 0 && distToW < shortestPath) {
      return w;
    }

    return ancestor;
  }

  public int length(Iterable<Integer> v, Iterable<Integer> w) {
    int ancestor = ancestor(v, w);
    if (ancestor == -1) {
      return -1;
    }
    BreadthFirstDirectedPaths bfs = new BreadthFirstDirectedPaths(DG, v);
    BreadthFirstDirectedPaths bfs1 = new BreadthFirstDirectedPaths(DG, w);
    return bfs.distTo(ancestor) + bfs1.distTo(ancestor);
  }

  public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
    BreadthFirstDirectedPaths bfs = new BreadthFirstDirectedPaths(DG, v);
    BreadthFirstDirectedPaths bfs2 = new BreadthFirstDirectedPaths(DG, w);

    int shortestW = -1;
    int shortestV = -1;
    TreeSet<Integer> distV = new TreeSet<>();
    for (Integer i : w) {
      if (bfs.hasPathTo(i)) {
        int x = bfs.distTo(i);
        if (distV.isEmpty() || x < distV.first()) {
          shortestV = i;
        }
        distV.add(x);
      }
    }
    TreeSet<Integer> distW = new TreeSet<>();
    for (Integer i : v) {
      if (bfs2.hasPathTo(i)) {
        int x = bfs2.distTo(i);
        if (distW.isEmpty() || x < distW.first()) {
          shortestW = i;
        }
        distW.add(x);
      }
    }

    int ancestor = -1;
    int shortestPath = Integer.MAX_VALUE;
    for (int i = 0; i < DG.V(); i++) {
      if (bfs.hasPathTo(i) && bfs2.hasPathTo(i)) {
        int distance = bfs.distTo(i) + bfs2.distTo(i);
        if (distance < shortestPath) {
          shortestPath = distance;
          ancestor = i;
        }
      }
    }

    if (shortestV != -1 && distV.first() < shortestPath) {
      return shortestV;
    } else if (shortestW != -1 && distW.first() < shortestPath) {
      return shortestW;
    }

    return ancestor;
  }
}

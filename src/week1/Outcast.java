package week1;

public class Outcast {
  private WordNet wordNet;

  public Outcast(WordNet wordNet) {
    this.wordNet = wordNet;
  }

  public String outcast(String[] nouns) {
    String result = "";
    int max = 0;
    for (String noun : nouns) {
      int distance = 0;
      for (String noun1 : nouns) {
        distance += wordNet.distance(noun, noun1);
      }
      if (distance > max) {
        max = distance;
        result = noun;
      }
    }
    return result;
  }
}

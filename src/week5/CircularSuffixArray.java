package week5;

import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;

public class CircularSuffixArray {
  private Integer[] array;
  private int length;

  public CircularSuffixArray(String s) {
    if (s == null) throw new IllegalArgumentException();
    length = s.length();
    array = new Integer[length];
    for (int i = 0; i < s.length(); i++) {
      array[i] = i;
    }
    Arrays.sort(array, (o1, o2) -> {
      int i1 = o1;
      int i2 = o2;
      char a = s.charAt(i1);
      char b = s.charAt(i2);
      while (a == b && i1 < s.length() - 1 + o1) {
        i1++;
        i2++;
        a = s.charAt(i1 % s.length());
        b = s.charAt(i2 % s.length());
      }
      return Integer.compare(a, b);
    });
  }

  public int length() {
    return length;
  }

  public int index(int i) {
    if (i > length - 1 || i < 0) throw new IllegalArgumentException();
    return array[i];
  }

  public static void main(String[] args) {
    CircularSuffixArray csa = new CircularSuffixArray("BABABAAABA");
    StdOut.println(csa.length);
    for (int i = 0; i < csa.length; i++) {
      StdOut.println(csa.index(i));
    }
  }
}

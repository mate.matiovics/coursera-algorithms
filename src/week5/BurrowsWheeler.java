package week5;

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

public class BurrowsWheeler {

  public static void transform() {
    String s = BinaryStdIn.readString();
    CircularSuffixArray csa = new CircularSuffixArray(s);
    int first = 0;
    char[] t = new char[s.length()];
    for (int i = 0; i < s.length(); i++) {
      int index = csa.index(i);
      if (index == 0) {
        first = i;
        index = s.length();
      }
      t[i] = s.charAt(index - 1);
    }
    BinaryStdOut.write(first, 32);
    for (char c : t) {
      BinaryStdOut.write(c);
    }
    BinaryStdOut.close();
  }

  public static void inverseTransform() {
    int first = BinaryStdIn.readInt(32);
    String s = BinaryStdIn.readString();

    char[] f = Arrays.copyOf(s.toCharArray(), s.length());
    Arrays.sort(f);

    Map<Character, Integer> map = new TreeMap<>();

    int[] next = new int[s.length()];
    for (int i = 0; i < f.length; i++) {
      char c = f[i];
      Integer lastIndex = map.get(c);
      int index;
      if (lastIndex != null) {
        index = s.indexOf(c, lastIndex + 1);
      } else {
        index = s.indexOf(c);
      }
      map.put(c, index);
      next[i] = index;
    }

    int k = next[first];
    int counter = 1;
    while (counter < next.length) {
      BinaryStdOut.write(s.charAt(k));
      k = next[k];
      counter++;
    }
    BinaryStdOut.write(s.charAt(first));

    BinaryStdOut.close();
  }

  public static void main(String[] args) {
    if (args[0].equals("-")) {
      transform();
    } else if (args[0].equals("+")) {
      inverseTransform();
    } else {
      throw new IllegalArgumentException();
    }
  }
}

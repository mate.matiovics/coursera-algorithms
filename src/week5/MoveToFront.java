package week5;

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;
import edu.princeton.cs.algs4.StdOut;

import java.util.LinkedList;

public class MoveToFront {
  private static final int R = 256;
  private static final int LG_R = 8;

  public static void encode() {
    LinkedList<Character> linkedList = new LinkedList<>();
    for (char i = 0; i < R; i++) {
      linkedList.add(i);
    }
    while (!BinaryStdIn.isEmpty()) {
      char c = BinaryStdIn.readChar();
      int i = linkedList.indexOf(c);

      BinaryStdOut.write(i, LG_R);
      linkedList.remove(i);
      linkedList.addFirst(c);
    }
    BinaryStdOut.close();
  }

  public static void decode() {
    LinkedList<Character> linkedList = new LinkedList<>();
    for (char i = 0; i < R; i++) {
      linkedList.add(i);
    }
    while (!BinaryStdIn.isEmpty()) {
      int i = BinaryStdIn.readInt(8);
      char c = linkedList.get(i);

      BinaryStdOut.write(c);
      linkedList.remove(i);
      linkedList.addFirst(c);
    }
    BinaryStdOut.close();
  }

  public static void main(String[] args) {
    if (args[0].equals("-")) {
      encode();
    } else if (args[0].equals("+")) {
      decode();
    } else {
      throw new IllegalArgumentException();
    }
  }
}

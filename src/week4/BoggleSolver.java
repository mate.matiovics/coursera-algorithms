package week4;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

import java.util.Set;
import java.util.TreeSet;

public class BoggleSolver {
  private Trie dictionary;

  public BoggleSolver(String[] dictionary) {
    this.dictionary = new Trie();
    for (String s : dictionary) {
      this.dictionary.add(s);
    }
  }

  public Iterable<String> getAllValidWords(BoggleBoard board) {
    DFS dfs = new DFS(board);
    return dfs.search();
  }

  public int scoreOf(String word) {
    if (!dictionary.contains(word)) {
      return 0;
    }
    if (word.length() >= 8) {
      return 11;
    } else if (word.length() == 7) {
      return 5;
    } else if (word.length() > 4) {
      return word.length() / 2;
    } else {
      return (word.length() - 1) / 2;
    }
  }

  private class DFS {
    private BoggleBoard board;
    private boolean visited[];

    public DFS(BoggleBoard board) {
      this.board = board;
    }

    public Set<String> search() {
      Set<String> result = new TreeSet<>();
      for (int i = 0; i < board.cols() * board.rows(); i++) {
        int[] coor = convert(i);
        int row = coor[0];
        int col = coor[1];
        this.visited = new boolean[board.cols() * board.rows()];
        result = search(result, row, col, new StringBuilder());
      }
      return result;
    }

    private Set<String> search(Set<String> result, int row, int col, StringBuilder b) {
      if (row < 0 || row >= board.rows() || col < 0 || col >= board.cols()) {
        return result;
      }

      int i = convert(row, col);
      if (visited[i]) {
        return result;
      }
      visited[i] = true;
      char c = board.getLetter(row, col);
      String s = b.append(c).toString();
      if (c == 'Q') {
        s = b.append('U').toString();
      }
      if (s.length() >= 3 && dictionary.contains(s)) {
        result.add(s);
      }
      if (dictionary.isPrefix(s)) {
        for (int[] next : adj(i)) {
          result = search(result, next[0], next[1], b);
        }
      }
      b.deleteCharAt(b.length() - 1);
      if (c == 'Q') {
        b.deleteCharAt(b.length() - 1);
      }
      visited[i] = false;

      return result;
    }

    private int[][] adj(int i) {
      int[][] adj = new int[8][2];

      int[] coor = convert(i);
      int row = coor[0];
      int col = coor[1];

      adj[0] = new int[] {row, col - 1};
      adj[1] = new int[] {row, col + 1};
      adj[2] = new int[] {row - 1, col - 1};
      adj[3] = new int[] {row - 1, col};
      adj[4] = new int[] {row - 1, col + 1};
      adj[5] = new int[] {row + 1, col - 1};
      adj[6] = new int[] {row + 1, col};
      adj[7] = new int[] {row + 1, col + 1};

      return adj;
    }

    private int convert(int row, int col) {
      return row * board.cols() + col;
    }

    private int[] convert(int i) {
      int[] coor = new int[2];
      coor[0] = i / board.cols();
      coor[1] = i % board.cols();
      return coor;
    }
  }

  public static void main(String[] args) {
    In in = new In(args[0]);
    String[] dictionary = in.readAllStrings();
    BoggleSolver solver = new BoggleSolver(dictionary);
    BoggleBoard board = new BoggleBoard(args[1]);
    int score = 0;
    for (String word : solver.getAllValidWords(board)) {
      StdOut.println(word);
      score += solver.scoreOf(word);
    }
    StdOut.println("Score = " + score);
  }
}

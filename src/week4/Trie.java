package week4;

public class Trie {
  private static final int R = 26;
  private static final int ADJ = 65;

  private Node root;
  private int n;

  private static class Node {
    private Node[] next = new Node[R];
    private boolean isString;
  }

  public Trie() {
  }

  public boolean contains(String key) {
    if (key == null) throw new IllegalArgumentException("argument to contains() is null");
    Node x = get(root, key, 0);
    if (x == null) return false;
    return x.isString;
  }

  private Node get(Node x, String key, int d) {
    if (x == null) return null;
    if (d == key.length()) return x;
    char c = key.charAt(d);
    return get(x.next[c - ADJ], key, d+1);
  }

  public void add(String key) {
    if (key == null) throw new IllegalArgumentException("argument to add() is null");
    root = add(root, key, 0);
  }

  private Node add(Node x, String key, int d) {
    if (x == null) x = new Node();
    if (d == key.length()) {
      if (!x.isString) n++;
      x.isString = true;
    }
    else {
      char c = key.charAt(d);
      x.next[c - ADJ] = add(x.next[c - ADJ], key, d+1);
    }
    return x;
  }

  public int size() {
    return n;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public void delete(String key) {
    if (key == null) throw new IllegalArgumentException("argument to delete() is null");
    root = delete(root, key, 0);
  }

  private Node delete(Node x, String key, int d) {
    if (x == null) return null;
    if (d == key.length()) {
      if (x.isString) n--;
      x.isString = false;
    }
    else {
      char c = key.charAt(d);
      x.next[c - ADJ] = delete(x.next[c - ADJ], key, d+1);
    }

    // remove subtrie rooted at x if it is completely empty
    if (x.isString) return x;
    for (int c = ADJ; c < R + ADJ; c++)
      if (x.next[c - ADJ] != null)
        return x;
    return null;
  }

  public boolean isPrefix(String key) {
    if (key == null) throw new IllegalArgumentException("argument to isPrefix() is null");
    Node x = get(root, key, 0);
    return x != null;
  }
}

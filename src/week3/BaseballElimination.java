package week3;

import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseballElimination {
  private int n;
  private ArrayList<String> teamNames;
  private Map<String, Team> teams;
  private FlowNetwork network;
  private FordFulkerson ff;
  private List<String> list;

  public BaseballElimination(String filename) {
    In in = new In(filename);
    this.teamNames = new ArrayList<>();
    this.n = Integer.parseInt(in.readLine());
    this.teams = new HashMap<>();
    for (String s : in.readAllLines()) {
      Team team = new Team(s);
      teams.put(team.name, team);
      teamNames.add(team.name);
    }
  }

  public int numberOfTeams() {
    return n;
  }

  public Iterable<String> teams() {
    return teamNames;
  }

  public int wins(String team) {
    Team x = teams.get(team);
    if (x == null) {
      throw new IllegalArgumentException();
    }
    return x.wins;
  }

  public int losses(String team) {
    Team x = teams.get(team);
    if (x == null) {
      throw new IllegalArgumentException();
    }
    return x.losses;
  }

  public int remaining(String team) {
    Team x = teams.get(team);
    if (x == null) {
      throw new IllegalArgumentException();
    }
    return x.remaining;
  }

  public int against(String team1, String team2) {
    Team one = teams.get(team1);
    Team two = teams.get(team2);
    if (one == null || two == null) {
      throw new IllegalArgumentException();
    }
    return one.matches[teamNames.indexOf(team2)];
  }

  public boolean isEliminated(String team) {
    Team x = teams.get(team);
    if (x == null) {
      throw new IllegalArgumentException();
    }
    int pw = x.wins + x.remaining;
    for (String name : teamNames) {
      if (!name.equals(team) && teams.get(name).wins > pw) {
        list = Collections.singletonList(name);
        return true;
      }
    }

    populateNetwork(team);
    for (FlowEdge next : network.edges()) {
      if (next.from() == 0 && next.capacity() - next.flow() != 0) {
        return true;
      }
    }
    return false;
  }

  public Iterable<String> certificateOfElimination(String team) {
    if (!isEliminated(team)) {
      return null;
    }
    return list;
  }

  private void populateNetwork(String team) {
    double noOfMatches = (n - 1) * ((double) (n - 2) / 2);
    network = new FlowNetwork(n - 1 + (int) noOfMatches + 2);
    Team x = teams.get(team);
    int k = 1;
    ArrayList<String> r = new ArrayList<>(teamNames);
    r.remove(team);
    for (int i = 0; i < r.size(); i++) {
      Team y = teams.get(r.get(i));
      for (int j = i + 1; j < r.size(); j++) {
        int left = y.matches[teamNames.indexOf(r.get(j))];
        network.addEdge(new FlowEdge(0, k, left));
        network.addEdge(new FlowEdge(k, 1 + (int) noOfMatches + i, Double.POSITIVE_INFINITY));
        network.addEdge(new FlowEdge(k++, 1 + (int) noOfMatches + j, Double.POSITIVE_INFINITY));
      }
      int w = x.wins + x.remaining - y.wins;
      network.addEdge(new FlowEdge(1 + (int) noOfMatches + i, network.V() - 1, w));
    }
    ff = new FordFulkerson(network, 0, network.V() - 1);
    list = new ArrayList<>();
    for (int i = 0; i < r.size(); i++) {
      if (ff.inCut(1 + (int) noOfMatches + i)) {
        list.add(r.get(i));
      }
    }
  }

  private class Team {
    private String name;
    private int wins;
    private int losses;
    private int remaining;
    private int[] matches;

    public Team(String input) {
      input = input.trim().replaceAll(" +", " ");
      String[] details = input.split(" ");
      this.name = details[0];
      this.wins = Integer.parseInt(details[1]);
      this.losses = Integer.parseInt(details[2]);
      this.remaining = Integer.parseInt(details[3]);
      this.matches = Arrays.stream(Arrays.copyOfRange(details, 4, details.length))
          .mapToInt(Integer::parseInt)
          .toArray();
    }

    public String toString() {
      return this.name;
    }
  }

  public static void main(String[] args) {
    BaseballElimination division = new BaseballElimination(args[0]);
    for (String team : division.teams()) {
      if (division.isEliminated(team)) {
        StdOut.print(team + " is eliminated by the subset R = { ");
        for (String t : division.certificateOfElimination(team)) {
          StdOut.print(t + " ");
        }
        StdOut.println("}");
      } else {
        StdOut.println(team + " is not eliminated");
      }
    }
  }
}

package week2;

import edu.princeton.cs.algs4.Picture;

public class SeamCarver {
  private Picture picture;
  private double[][] energies;

  public SeamCarver(Picture picture) {
    if (picture == null) {
      throw new IllegalArgumentException();
    }
    this.picture = new Picture(picture);
    energies = new double[picture.height()][picture.width()];
    for (int i = 0; i < energies.length; i++) {
      for (int j = 0; j < energies[i].length; j++) {
        energies[i][j] = calculateEnergy(j, i);
      }
    }
  }

  public Picture picture() {
    return new Picture(picture);
  }

  public int width() {
    return picture.width();
  }

  public int height() {
    return picture.height();
  }

  public double energy(int x, int y) {
    if (x < 0 || x >= picture.width() || y < 0 || y >= picture.height()) {
      throw new IllegalArgumentException();
    }
    return energies[y][x];
  }

  private double calculateEnergy(int x, int y) {
    if (x < 0 || x >= picture.width() || y < 0 || y >= picture.height()) {
      throw new IllegalArgumentException();
    }
    if (x == 0 || x == picture.width() - 1 || y == 0 || y == picture.height() - 1) {
      return 1000;
    }
    int rightColor = picture.getRGB(x + 1, y);
    int leftColor = picture.getRGB(x - 1, y);
    int upperColor = picture.getRGB(x, y - 1);
    int lowerColor = picture.getRGB(x, y + 1);

    int rx = ((rightColor >> 16) & 0xFF) - ((leftColor >> 16) & 0xFF);
    int gx = ((rightColor >> 8) & 0xFF) - ((leftColor >> 8) & 0xFF);
    int bx = ((rightColor) & 0xFF) - ((leftColor) & 0xFF);

    int ry = ((lowerColor >> 16) & 0xFF) - ((upperColor >> 16) & 0xFF);
    int gy = ((lowerColor >> 8) & 0xFF) - ((upperColor >> 8) & 0xFF);
    int by = ((lowerColor) & 0xFF) - ((upperColor) & 0xFF);

    double deltaX = Math.pow(rx, 2) + Math.pow(gx, 2) + Math.pow(bx, 2);
    double deltaY = Math.pow(ry, 2) + Math.pow(gy, 2) + Math.pow(by, 2);
    return Math.sqrt(deltaX + deltaY);
  }

  public int[] findVerticalSeam() {
    double[][] doubles = new double[energies.length][];
    for (int i = 0; i < doubles.length; i++) {
      doubles[i] = energies[i].clone();
    }
    int[][] indexes = new int[doubles.length][doubles[0].length];
    int lastIndex = 0;
    double min = Integer.MAX_VALUE;
    for (int i = 1; i < doubles.length; i++) {
      for (int j = 0; j < doubles[i].length; j++) {
        double m = doubles[i - 1][j];
        int mIndex = j;
        if (j > 0 && doubles[i - 1][j - 1] < m) {
          m = doubles[i - 1][j - 1];
          mIndex = j - 1;
        }
        if (j < doubles[i].length - 1 && doubles[i - 1][j + 1] < m) {
          m = doubles[i - 1][j + 1];
          mIndex = j + 1;
        }
        doubles[i][j] = energies[i][j] + m;
        indexes[i][j] = mIndex;
        if (i == doubles.length - 1 && doubles[i][j] < min) {
          min = doubles[i][j];
          lastIndex = j;
        }
      }
    }
    int[] result = new int[picture.height()];
    result[result.length - 1] = lastIndex;
    for (int i = result.length - 2; i >= 0; i--) {
      result[i] = indexes[i + 1][result[i + 1]];
    }

    return result;
  }

  public int[] findHorizontalSeam() {
    double[][] doubles = new double[energies.length][energies[0].length];
    for (int i = 0; i < doubles.length; i++) {
      doubles[i] = energies[i].clone();
    }
    int[][] indexes = new int[doubles.length][doubles[0].length];
    int lastIndex = 0;
    double min = Integer.MAX_VALUE;
    for (int i = 1; i < doubles[0].length; i++) {
      for (int j = 0; j < doubles.length; j++) {
        double m = doubles[j][i - 1];
        int mIndex = j;
        if (j > 0 && doubles[j - 1][i - 1] < m) {
          m = doubles[j - 1][i - 1];
          mIndex = j - 1;
        }
        if (j < doubles.length - 1 && doubles[j + 1][i - 1] < m) {
          m = doubles[j + 1][i - 1];
          mIndex = j + 1;
        }
        doubles[j][i] = energies[j][i] + m;
        indexes[j][i] = mIndex;
        if (i == doubles[0].length - 1 && doubles[j][i] < min) {
          min = doubles[j][i];
          lastIndex = j;
        }
      }
    }
    int[] result = new int[picture.width()];
    result[result.length - 1] = lastIndex;
    for (int i = result.length - 2; i >= 0; i--) {
      result[i] = indexes[result[i + 1]][i + 1];
    }

    return result;
  }

  public void removeHorizontalSeam(int[] seam) {
    if (picture.height() <= 1) {
      throw new IllegalArgumentException();
    }
    validateHorizontalSeam(seam);
    double[][] newEnergies = new double[picture.height() - 1][picture.width()];
    Picture newPicture = new Picture(picture.width(), picture.height() - 1);

    for (int i = 0; i < picture.width(); i++) {
      int index = 0;
      for (int j = 0; j < picture.height() - 1; j++) {
        if (seam[i] == index) {
          index++;
        }
        newEnergies[j][i] = energies[index][i];
        newPicture.setRGB(i, j, picture.getRGB(i, index++));
      }
    }

    picture = newPicture;

    for (int i = 0; i < newEnergies[0].length; i++) {
      if (seam[i] > 0) {
        newEnergies[seam[i] - 1][i] = calculateEnergy(i, seam[i] - 1);
      }
      newEnergies[seam[i]][i] = calculateEnergy(i, seam[i]);
    }

    energies = newEnergies;
  }

  public void removeVerticalSeam(int[] seam) {
    if (picture.width() <= 1) {
      throw new IllegalArgumentException();
    }
    validateVerticalSeam(seam);
    double[][] newEnergies = new double[picture.height()][picture.width() - 1];
    Picture newPicture = new Picture(picture.width() - 1, picture.height());

    for (int i = 0; i < picture.height(); i++) {
      int index = 0;
      for (int j = 0; j < picture.width() - 1; j++) {
        if (seam[i] == index) {
          index++;
        }
        newEnergies[i][j] = energies[i][index];
        newPicture.setRGB(j, i, picture.getRGB(index++, i));
      }
    }

    picture = newPicture;

    for (int i = 0; i < newEnergies.length; i++) {
      if (seam[i] > 0) {
        newEnergies[i][seam[i] - 1] = calculateEnergy(seam[i] - 1, i);
      }
      newEnergies[i][seam[i]] = calculateEnergy(seam[i], i);
    }

    energies = newEnergies;
  }

  private void validateVerticalSeam(int[] seam) {
    if (seam == null || seam.length != picture.height()) {
      throw new IllegalArgumentException();
    }
    for (int i = 0; i < seam.length; i++) {
      if (seam[i] < 0 || seam[i] >= picture.width()) {
        throw new IllegalArgumentException();
      }
      if (i > 0) {
        if (Math.abs(seam[i] - seam[i - 1]) > 1) {
          throw new IllegalArgumentException();
        }
      }
    }
  }

  private void validateHorizontalSeam(int[] seam) {
    if (seam == null || seam.length != picture.width()) {
      throw new IllegalArgumentException();
    }
    for (int i = 0; i < seam.length; i++) {
      if (seam[i] < 0 || seam[i] >= picture.height()) {
        throw new IllegalArgumentException();
      }
      if (i > 0) {
        if (Math.abs(seam[i] - seam[i - 1]) > 1) {
          throw new IllegalArgumentException();
        }
      }
    }
  }

  public static void main(String[] args) {
    Picture picture = new Picture("/HJocean.png");

    picture.show();
    SeamCarver carver = new SeamCarver(picture);
    for (int i = 0; i < 80; i++) {
      carver.removeVerticalSeam(carver.findVerticalSeam());
      carver.removeHorizontalSeam(carver.findHorizontalSeam());
    }
    carver.picture.show();
  }
}

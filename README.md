# coursera-algorithms

My solutions for the assignments of the following course on Coursera: Algorithms, Part II by Princeton University
(The project requires an external .jar dependency available on https://algs4.cs.princeton.edu/code/ to compile.)